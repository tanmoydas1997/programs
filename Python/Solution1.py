T=int(input())
for j in range(T):
    n=int(input().strip())
    m=list(map(int,input().strip().split()))
    a=[]
    l=[]
    for i in range(0,len(m),2):
        a.append(m[i])
    for i in range(1,len(m),2):
        l.append(m[i])
    
    a=sorted(a)
    l=sorted(l)
    flag=0
    loc=len(m)+114
    loc2=len(m)+114
    for i in range(len(l)):
        if(a[i]>l[i]):
            flag=1
            loc=2*i
            break
    for i in range(1,len(a)):
        if(l[i-1]>a[i]):
            flag=1
            loc2=2*i-1
            break
    if(flag==0):
        print('Case #'+str(j+1)+': OK')
    else:
        if(loc<loc2):
            print('Case #'+str(j+1)+': '+str(loc))
        else:
            print('Case #'+str(j+1)+': '+str(loc2))
        