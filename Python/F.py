r,c,n=map(int,raw_input().split())
l=[]
for i in xrange(r):
    l.append(map(int,raw_input().split()))
S=[[0 for i in xrange(c+1)] for i in xrange(r+1)]
S[1][1]=l[0][0]
for i in xrange(2,r+1):
    S[1][i]=S[1][i-1]+l[0][i-1]
for i in xrange(2,c+1):
    S[i][1]=S[i-1][1]+l[i-1][0]
for i in xrange(2,r+1):
    for j in xrange(2,c+1):
        S[i][j]=l[i-1][j-1]+S[i-1][j]+S[i][j-1]-S[i-1][j-1]
for i in xrange(n):
    r1,c1,r2,c2=map(int,raw_input().split())
    print S[r2+1][c2+1]+S[r1][c1]-S[r1][c2+1]-S[r2+1][c1]

