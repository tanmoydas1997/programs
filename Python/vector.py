from math import sqrt,acos
class Vector(object):
    def __init__(self, coordinates):
        try:
            if not coordinates:
                raise ValueError
            self.coordinates = tuple(coordinates)
            self.dimension = len(coordinates)

        except ValueError:
            raise ValueError('The coordinates must be nonempty')

        except TypeError:
            raise TypeError('The coordinates must be an iterable')


    def __str__(self):
        return 'Vector: {}'.format(self.coordinates)


    def __eq__(self, v):
        return self.coordinates == v.coordinates
    def plus(self,v):
        new_coordinates=[x+y for x,y in zip(self.coordinates,v.coordinates)]
        return(Vector(new_coordinates))

    def minus(self,v):
        new_coordinates=[x-y for x,y in zip(self.coordinates,v.coordinates)]
        return(Vector(new_coordinates))

    def scalar(self,c):
        new_coordinates=[c*x for x in self.coordinates]
        return(Vector(new_coordinates))

    def magnitude(self):
        mag=[x**2 for x in self.coordinates]        
        return(sqrt(sum(mag)))

    def direction(self):
        mag=self.magnitude()
        return(self.scalar(1./mag)) 
    def inner_product(self,v):
        dot_product=[x*y for x,y in zip(self.coordinates,v.coordinates)]
        return(sum(dot_product))
    def angle(self,v):
        dot_product=dot_product(self,v)
        mag_multiply=self.magnitude()*v.magnitude()
        theta=acos(dot_product/mag_multiply)
    def check_parallel(self,v):
        

my_vector=Vector([1,2,3])
print(my_vector.magnitude())
print(my_vector.direction())

