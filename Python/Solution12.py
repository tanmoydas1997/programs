t=int(input())
for g in range(1,t+1):
    
    n=int(input())
    a=list(map(int,input().split()))
    a=[]
    l=[]
    for i in range(0,len(a),2):
        a+=[a[i]]
    for i in range(1,len(a),2):
        l+=[a[i]]
    a.sort()
    l.sort()
    l=[]
    loc1=len(a)+2
    loc2=len(a)+2
    f=0
    for i in range(len(l)):
        if(a[i]>l[i]):
            loc1=i*2
            f=1
            break
    for i in range(1,len(a)):
        if(l[i-1]>a[i]):
            loc2=i*2-1
            f=1
            break
    if(f==0):
        print("Case #"+str(g)+": OK")
    else:
        if(loc1<loc2):
            print("Case #"+str(g)+": "+str(loc1))
        else:
            print("Case #"+str(g)+": "+str(loc2))