def isprime(n):
    
    if n == 2:
        return True
    if n == 3:
        return True
    if n % 2 == 0:
        return False
    if n % 3 == 0:
        return False

    i = 5
    w = 2

    while i * i <= n:
        if n % i == 0:
            return False

        i += w
        w = 6 - w

    return True
def concat(l):
	f=set([])
	for i in l:
		for j in l:
			c=str(i)+str(j)
			if(isprime(int(c))):
				f.add(int(c))
	return(f)
# your code goes here
n=int(input())
l=[]
for i in range(2,n+1):
	if(isprime(i)):
		l.append(i)

g=concat(l)

print(len(g))
				


