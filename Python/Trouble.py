def TroubleSort(L):
    l=[]
    done= False
    while(not done):
      done = True
      for i in range(len(L)-2):
        if L[i] > L[i+2]:
          done = False
          l=L[i:i+2+1]
          l.reverse()
          L=L[:i]+l+L[i+3:]
    return L
T=int(input())
for j in range(T):
    n=int(input())
    a=input()
    l=list(map(int,a.split()))
    L=TroubleSort(l)
    print(L)
    flag=0
    for i in range(0,len(L)-1):
        if(L[i]>L[i+1]):
            loc=i-1
            flag=1
    if (flag!=0):
        print('Case #'+str(j+1)+': '+str(loc))
    else:
        print('Case #'+str(j+1)+': OK')

