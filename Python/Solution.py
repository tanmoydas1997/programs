def val(r):
    ch=1
    sum=0
    for i in range(len(r)):
        if(r[i]=='S'):
            sum=sum+ch
        else:
            ch=ch*2
    return(sum)

def swap(r):
    for i in range(len(r)-1,0,-1):
        if(r[i]=='S' and r[i-1]=='C'):
            return(r[0:i-1]+'S'+'C'+r[i+1:])
    return('no')

T=int(input())
for i in range(1,T+1):
    m,k=input().strip().split(' ')
    n=int(m)
    count=0
    s=0
    while(val(k)>n and k!='no'):
        s=k
        k=swap(k)
        if(k!='no'):
            count+=1
    if(k=='no'):
        if(val(s)>n):
            print('Case #'+str(i)+': IMPOSSIBLE')
        else:
            print('Case #'+str(i)+': '+str(count))
    else:
        print('Case #'+str(i)+': '+str(count))


