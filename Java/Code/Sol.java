import java.util.*;
class Solution
{
    public static long val(String x)
    {
        long sum=0;long pow=1;
        for(int i=0;i<x.length();i++)
        {
            if(x.charAt(i)=='S')
            {
                sum+=pow;
            }
            else
            pow*=2;
            
        }
        
        return(sum);
    }
    public static void main(String args[])
    {
        Scanner sc=new Scanner(System.in);
        int T=sc.nextInt();
        for(int i=1;i<=T;i++)
        {
            long D=sc.nextLong();
            String s=sc.next();
            long count=0;int j=0;
            
            while(val(s)>D)
            {
                    int h=0;
                    for(int k=0;k<=s.lastIndexOf('S');k++)
                    {
                        if(s.charAt(k)=='S')
                        {
                            h++;
                        }
                    }
                    
                    if(h==s.lastIndexOf('S')+1)
                    break;
                    for(;j<s.length();j++)
                    {
                        if(s.charAt(j)=='S')
                        break;
                    }
                    
                    if(j>0 && j<s.length() && s.charAt(j-1)!='S')
                    {
                         s=s.substring(0,j-1)+"SC"+s.substring(j+1,s.length());
                         count++;;
                         
                    }
                    j=j+1;
                    if(j>=s.length())
                    {
                        j=0;
                        
                    }
            }   

            if(val(s)>D)
            System.out.println("Case #"+i+": IMPOSSIBLE");
            else
            System.out.println("Case #"+i+": "+count);
        }
    }
}