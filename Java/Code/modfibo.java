import java.util.*;


class Solution
{
    static Long product(LinkedList<Long> q)
    {
        long p=1;
        for (Long temp:q)
            {
                p=p*temp%(long)(Math.pow(10,9)+7);
            } 
        return p;
    }
    
    public static void main(String args[])
    {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        for(int i=0;i<t;i++)
            {
                int n=sc.nextInt();
                int k=sc.nextInt();
                long p=1;
                LinkedList<Long> queue=new LinkedList<Long>();
                for(int j=0;j<k;j++)
                    {
                        queue.add(sc.nextLong());
                        
                    }
                //disp(queue);
                //System.out.println();
                p=product(queue);
                int a=k;
                while(a<n-1)
                    {
                        queue.remove();
                        queue.add(p);
                       // disp(queue);
                        p=product(queue);
                       // System.out.println("p="+p);
                        a++;
                    }
                System.out.println(p);
            }
            sc.close();
    }
}