
import java.util.*;
import java.lang.*;
import java.io.*;

/* Name of the class has to be "Main" only if the class is public. */
class Codechef
{
	public static void main (String[] args) throws java.lang.Exception
	{
      int a[]={10,8,7,6,5};
      for(int i=0;i<a.length-1;i++)
        {
            int min=i;
            for(int j=i+1;j<a.length;j++)
                {
                    if(a[j]<a[min])
                        min=j;
                }
            int temp=a[i];
            a[i]=a[min];
            a[min]=temp;

            for(int k=0;k<a.length;k++)
                {
                    System.out.print(a[k]+"\t");
                }
                System.out.println();
        }
        for(int i=0;i<a.length;i++)
            {
                System.out.print(a[i]+"\t");
            }

      
	}
}