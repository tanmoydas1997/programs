import java.util.HashSet;
import java.util.*;

class Solution {

    static int find_hand(char c) {
        if (c == 'd' || c == 'f')// 0 for left and 1 for right
            return 0;
        else
            return 1;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        HashSet<String> h = new HashSet<String>();

        int T = Integer.parseInt(sc.next());
        for (int i = 0; i < T; i++) {
            int n = Integer.parseInt(sc.next());
            int hand = 0; // 0 for left and 1 for right
            int time = 0;
            for (int j = 0; j < n; j++) {
                String w = sc.next();
                if (h.contains(w)) {
                    int l = w.length();
                    char c = w.charAt(0);
                    hand = find_hand(c);
                    int t = 0;
                    t = t + 2;
                    for (int k = 1; k < l; k++) {
                        char d = w.charAt(k);
                        int hh = find_hand(d);
                        if (hh == hand)
                            t += 4;
                        else
                            t += 2;
                        hand = hh;

                    }
                    time = time + (t / 2);
                    System.out.println(time);
                    continue;
                }

                else {
                    h.add(w);
                    int l = w.length();
                    char c = w.charAt(0);
                    hand = find_hand(c);
                    time = time + 2;
                    for (int k = 1; k < l; k++) {
                        char d = w.charAt(k);
                        int hh = find_hand(d);
                        if (hh == hand)
                            time += 4;
                        else
                            time += 2;
                        hand = hh;
                        System.out.println(time);
                    }

                }
            }
            System.out.println(time);
        }

    }
}