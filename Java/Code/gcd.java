import java.util.Scanner;
class GreatestCommonDivisor
{
   private long calculateGCD(long a,long b)//Euclidean Algorithm
    {
        if(b==0)
        {
            return(a);
        }
        long ap=a%b;
        
        return(calculateGCD(b, ap));
        
    }
    private long calculateLCM(long a,long b)
    {
        if(b==0)
         return(1);
        
        long gcd=calculateGCD(a, b);
        long lcm=a*b/gcd;
        return lcm;
    }
    private long calculateFibonacci(int n)
    {
        long F[]=new long[n];
       F[0]=0;
       F[1]=1;
        for(int i=2;i<n;i++)
        {
            F[i]=F[i-1]+F[i-2];
            //System.out.println(F[i]);
        }
        return F[n-1];
        
    }
    private long calculateFibonacciLastDigit(int n)
    {
        long F[]=new long[n];
        F[0]=0;
        F[1]=1;
        for(int i=2;i<n;i++)
        {
            
            F[i]=F[i-1]%10+F[i-2]%10;
            if(!(F[i]<10))
            {
                F[i]=F[i]%10;
            }
            //System.out.println(F[i]);
        }
        return F[n-1];
        
    }
     long calculateFibonacciModm(int n,int m)
    {
        long F[]=new long[n];
        long A[]=new long[n];
        F[0]=0;
        F[1]=1;
        A[0]=0;
        A[1]=1;
        int p=0;
        int f=1;
      for(int i=2;i<n;i++)
      {
          F[i]=F[i-1]+F[i-2];
          A[i]=F[i]%m;
          //System.out.println("F[i]="+F[i]);
          System.out.print(A[i]);

          if(A[i]==1 && A[i-1]==0)
          {
              p=i-1;
              break;
          }
      }
      System.out.println("Period="+p);
      int index=n%p;

      
        return F[index];
        
    }
    void displayGCD(long a,long b)
    {
        System.out.println("The gcd of"+a+" and "+b+" is "+calculateGCD(a,b));
    }
    void displayFibonacci(int n)
    {
        System.out.println("The nth Fibonacci number is"+calculateFibonacci(n));
    }
    void displayFibonacciLastDigit(int n)
    {
        System.out.println("The last digit of nth Fibonacci number is"+calculateFibonacciLastDigit(n));
    }
    void displayLCM(long a,long b)
    {
        System.out.println("The LCM of"+a+" and "+b+" is "+calculateLCM(a,b));
    }

}
class Menu
{
public static void main(String args[])
{
Scanner sc=new Scanner(System.in);
GreatestCommonDivisor ob=new GreatestCommonDivisor();
/*ob.displayGCD(357, 234);
ob.displayLCM(28851538, 1183019);*/
ob.displayFibonacciLastDigit(8);//giving 132nd number
//System.out.println(ob.calculateFibonacciModm(239, 134));
}
}
