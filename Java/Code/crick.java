/*Write a member function for batsman class which will take one integer as parameter.Add this integer with total run,increment number of innings
and calculate other variables accordingly.Write a member function for bowler class which will take two integers as parameter.Add one integer 
with runs given and another with wickets.Increment number of innings and calculate other variables accordingly.*/

import java.util.*;

class Batsman
{
	
	int run;
	int n;
	int avg;
	void mem(int a)
	{
		run+=a;
		++n;
		avg=run/n;
	}
	void display(int a)
	{
		System.out.println("Batsman No."+a);
		System.out.println("Total Runs="+run);
		System.out.println("No of innings="+n);
		System.out.println("Average="+avg);
	}
		
}
	
class Bowler
{
	int wickets;
	int run_given;
	
	int n;
	void mem1(int a,int b)
	{
		run_given+=a;
		wickets+=b;
		n++;
	}
	void display(int a)
	{
		System.out.println("Bowler No."+a);
		System.out.println("Total Runs given="+run_given);
		System.out.println("No of wickets="+wickets);
		System.out.println("No of innings="+n);
	}
}

class Dis
{
	int a;
	int b;
	int d;


	void getbat()						//Taking inputs of Batsman class
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter runs by batsman");
		a=sc.nextInt();
	}
	void getbowl()						//Taking inputs of Bowler class
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter runs given by bowler");		
		b=sc.nextInt();
		System.out.println("Enter wickets by bowler");
		d=sc.nextInt();
	}
		
	public static void main(String args[])
	{
		Scanner sc=new Scanner(System.in);
		
		
		Batsman ob1=new Batsman();
		Batsman ob2=new Batsman();
		Bowler ob3=new Bowler();
		Bowler ob4=new Bowler();
		
		int f=1;
		
		
		while(f==1)
		{
			System.out.println("Enter 1.Batsman 2.Bowler");
			int c=sc.nextInt();
		switch(c)
		{
			
			case 1:
			{
				System.out.println("Enter 1.First Batsman 2.Second Batsman");
				int ch=sc.nextInt();
				switch(ch)
				{
				case 1:
				{
					Dis ob=new Dis();
					ob.getbat();
					
					ob1.mem(ob.a);		
					ob1.display(1);
					
					
					break;
				}
				case 2:
				{
					Dis ob=new Dis();
					ob.getbat();
					
					ob2.mem(ob.a);		
					ob2.display(2);
					
					break;
				}
				
				}
				System.out.println("Enter 2 to exit");
					f=sc.nextInt();
				break;
			}

			
			case 2:
			{
				
				System.out.println("Enter 1.First Bowler 2.Second Bowler");
				int ch=sc.nextInt();
				switch(ch)
				{
				case 1:
				{
					Dis ob=new Dis();
					ob.getbowl();
					
					ob3.mem1(ob.b,ob.d);		
					ob3.display(1);
					
					break;
				}
				case 2:
				{
					Dis ob=new Dis();
					ob.getbowl();
					
					ob4.mem1(ob.b,ob.d);		
					ob4.display(2);
					
					break;
				}
				
			}
				System.out.println("Enter 2 to exit");
					f=sc.nextInt();
			break;
			}
		
		}
	}	
		
	
	
		
	}
}
		
		
		
