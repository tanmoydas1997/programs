/*Write a member function for batsman class which will take one integer as parameter.Add this integer with total run,increment number of innings
and calculate other variables accordingly.Write a member function for bowler class which will take two integers as parameter.Add one integer 
with runs given and another with wickets.Increment number of innings and calculate other variables accordingly.*/

import java.util.*;

class Batsman
{
	
	int run;
	int n;
	int avg;
	void mem(int a)
	{
		run+=a;
		++n;
		avg=run/n;
	}
	void display(int a)
	{
		System.out.println("Batsman No."+a);
		System.out.println("Total Runs="+run);
		System.out.println("No of innings="+n);
		System.out.println("Average="+avg);
	}
		
}
	
class Bowler
{
	int wickets;
	int run_given;
	
	int n;
	void mem1(int a,int b)
	{
		run_given+=a;
		wickets+=b;
		n++;
	}
	void display(int a)
	{
		System.out.println("Bowler No."+a);
		System.out.println("Total Runs given="+run_given);
		System.out.println("No of wickets="+wickets);
		System.out.println("No of innings="+n);
	}
}

class Dis
{
	int a;
	int b;
	int d;


	void getbat()                                                //Taking inputs of Batsman class
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter runs by batsman");
		a=sc.nextInt();
	}
	void getbowl()                                               //Taking inputs of Bowler class
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter runs given by bowler");		
		b=sc.nextInt();
		System.out.println("Enter wickets by bowler");
		d=sc.nextInt();
	}
		
	public static void main(String args[])                       //main function
	{
		Scanner sc=new Scanner(System.in);                       //Scanner class object declaration and initialization
		System.out.println("Enter number of batsman & bowlers");		
		int n=sc.nextInt();                                      //No. of Batsman and Bowlers
		Batsman ob1[]=new Batsman[n];                            //Declaration & Instantiation of Batsman objects
		Bowler ob2[]=new Bowler[n];                              //Declaration & Instantiation Bowler objects
		
		
        for(int i=0;i<n;i++)                                     //Initializing Batsman & Bowler Objects
        {
            ob1[i]=new Batsman();
            ob2[i]=new Bowler();
        }
		int c=0;
		
		do
		{
			System.out.println("Enter 1.Batsman 2.Bowler 3.Display All and Exit");
		    c=sc.nextInt();
            switch(c)                                            //Switch Case to choose between Batsman and Bowler
            {
                
                case 1:                                          //Entering data for appropriate Batsman 
                {
                    System.out.println("Enter Batsman No. : ");
                    int k=sc.nextInt();
                    
                    Dis ob=new Dis();                            //Dis class object 
                    ob.getbat();
                        
                    ob1[k-1].mem(ob.a);		
                    ob1[k-1].display(k);	

                    

                    break;
                }

                
                case 2:                                           //Entering data for appropriate Bowler
                {
                    
                    System.out.println("Enter Bowler No. : ");
                    int k=sc.nextInt();
                    
                    Dis ob=new Dis();
                    ob.getbowl();
                    
                    ob2[k-1].mem1(ob.b,ob.d);		
                    ob2[k-1].display(k);
            
                    
                
                    break;
                }
                
                case 3:                                           //Display all stored data before exit          
                {
                    for(int i=0;i<n;i++)
                    {
                        ob1[i].display(i+1);
                    }
                    for(int i=0;i<n;i++)
                    {
                        ob2[i].display(i+1);
                    }
                    break;
                }
            
                default:
                {
                    System.out.println("Enter valid input!!!");
                }
            
            }
	    }
        while(c!=3)	;
		
	
	
		
	}
}
		
		