/* package codechef; // don't place package name! */

import java.util.*;
import java.lang.*;
import java.io.*;

/* Name of the class has to be "Main" only if the class is public. */
class Codechef
{
	public static void main (String[] args) throws java.lang.Exception
	{
		BufferedReader b=new BufferedReader(new InputStreamReader(System.in));
		int t=Integer.parseInt(b.readLine());
		for(int i=0;i<t;i++)
		{
		    String s=b.readLine();
		    long n=1;long max=1;long min=10000000;
		    for(int j=0;j<s.length();j++)
		    {
		        
		        if(s.charAt(j)=='<')
		        n++;
		        else if(s.charAt(j)=='>')
		        n--;
		        if(n>max)
		        max=n;
		        if(n<min)
		        min=n;
		       
		    } 
		    if(min<=0)
		    max=max+1-min;
		     System.out.println(max);
		}
		
	}
}