import java.util.Scanner;
class Solution
{
    public String swap(String str,int i,int j)
    {
        if (j == str.length() - 1)
            return str.substring(0, i) + str.charAt(j)
             + str.substring(i + 1, j) + str.charAt(i);
 
        return str.substring(0, i) + str.charAt(j)
               + str.substring(i + 1, j) + str.charAt(i) 
               + str.substring(j + 1, str.length());
    } 
    public long[] val(String s)
    {
        long sh=0;
        long ch=1;
        long sum=0;
        for(int i=0;i<s.length();i++)
        {
            char c=s.charAt(i);
            if(c=='C')
            {
                ch=ch*2;
            }
            else if(c=='S')
            {
                sum=sum+ch;
                sh=ch;
                
            }
        }
        //System.out.println("ch="+ch);
        long arr[]={sh,ch,sum};
        return arr;

    }
    
    public static void main(String args[])
    {
        Scanner sc=new Scanner(System.in);
        Solution ob=new Solution();
        int T=Integer.parseInt(sc.nextLine());
        for(long i=0;i<T;i++)
        {
            long D=Long.parseLong(sc.next());
            String P=sc.next();
            long count=0;
            long arr[]=ob.val(P);
            long sh=arr[0];

            long ch=arr[1];
            long sum=arr[2];
            //System.out.println("sum="+sum);
            String P1="";
            P1=P;
            String output="";
            long flag=0;
            //System.out.println("v="+v);
            while(sum>D)
            {

                //System.out.println("sum= "+sum+"D= "+D);
                //System.out.println("P1="+P1);
                int in=P.indexOf('C');
                if(in==-1)
                {
                    count=0;
                    output="IMPOSSIBLE";
                    break;
                }
                if(P.charAt(in+1)!='C')
                {
                    P= ob.swap(P,in,in+1);
                    count++;
                }
                else
                {
                    try{
                    P= ob.swap(P,in,in+2);
                    }
                    catch(Exception E)
                    {
                        break;
                    }
                    count=count+2;
                }
                //System.out.println("P="+P);
                if(P.compareTo(P1)==0)
                {
                    flag++;
                }
                //System.out.println("FLAG="+flag);
                if(flag==3)
                {
                output="IMPOSSIBLE";
                break;
                }
                
                P1=P;
                sum=ob.val(P)[2];
                

            }
            if(output.compareTo("IMPOSSIBLE")!=0)
            {
                output=Long.toString(count);
            }
            //
            System.out.println("Case #"+(i+1)+": "+output);
            //System.out.println(D+"  "+P);

        }

    }
}