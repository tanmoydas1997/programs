import java.util.*;
import java.io.*;

class Solution
{
     Long product(LinkedList<Long> q)
    {
        long p=1;
        for (long temp:q)
            {
                p=p*temp%(long)(Math.pow(10,9)+7);
            } 
        return p;
    }
    static void disp(LinkedList<Long> q)
    {
        long p=1;
        for (Long temp:q)
            {
                System.out.print(temp+" ");
            } 
        
    }
    public static void main(String args[])throws IOException
    {
        Scanner sc=new Scanner(System.in);
        BufferedReader in =new BufferedReader(new InputStreamReader(System.in));
        Solution ob=new Solution();
        int t=Integer.parseInt(in.readLine());
        for(int i=0;i<t;i++)
            {
                
                String b[]=in.readLine().split(" ");
                int n=Integer.parseInt(b[0]);
                int k=Integer.parseInt(b[1]);
                String ar[]=in.readLine().split(" ");
                long p=1;
                LinkedList<Long> queue=new LinkedList<Long>();
                for(int j=0;j<k;j++)
                    {   
                        long e=Long.parseLong(ar[j]);
                        queue.add(e);
                        
                    }
                //disp(queue);
                //System.out.println();
                p=ob.product(queue);
                int a=k;
                while(a<n-1)
                    {
                        queue.remove();
                        queue.add(p);
                       // disp(queue);
                        p=ob.product(queue);
                       // System.out.println("p="+p);
                        a++;
                    }
                    ob.disp(queue);
                    p=ob.product(queue);
                System.out.println(p);
            }
            
    }
}